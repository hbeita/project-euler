class EulerProject
	
	#-----------------------------------------------
	# 						P R O B L E M   1
	#-----------------------------------------------
	#@statement: If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9.
	# => The sum of these multiples is 23. Find the sum of all the multiples of 3 or 5 below 1000.
	def solve_problem1(array)
		array.find_all{|x| x < array.last  && (x%3 == 0 || x%5 == 0)}.reduce(:+)
	end

	#-----------------------------------------------
	# 						P R O B L E M   2
	#-----------------------------------------------
	#@statement: Each new term in the Fibonacci sequence is generated by adding the previous two terms.
	# => By starting with 1 and 2, the first 10 terms will be:
	# => 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...
	# => By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms
	def solve_problem2(max_num)
		fibonacci_secuence = [1,2]
		sum                = 0
		while fibonacci_secuence[fibonacci_secuence.length-2] + fibonacci_secuence[fibonacci_secuence.length-1] < max_num do
			fibonacci_secuence << fibonacci_secuence[fibonacci_secuence.length-2] + fibonacci_secuence[fibonacci_secuence.length-1]
		end
			fibonacci_secuence.each {|num| sum += num if num.even? }
			sum
	end
end