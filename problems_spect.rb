require 'minitest/autorun'
require_relative 'problems'

class TestEulerProject < MiniTest::Unit::TestCase
  def test_problem1
		@euler_project = EulerProject.new
    assert_equal 23, @euler_project.solve_problem1((1..10)), "Something in the method is not right!, should return 23"
    assert_equal 233168, @euler_project.solve_problem1((1..1000)), "Something in the method is not right!, should return 233168"
  end

  def test_problem2
		@euler_project = EulerProject.new
    assert_equal 4613732, @euler_project.solve_problem2(4000000), "The fibonachi secuence isn't sum 4613732"
  end

end